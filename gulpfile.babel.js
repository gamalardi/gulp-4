'use strict';

import { src, dest, watch, parallel, series, lastRun, task } from 'gulp';
import babelify from 'babelify';
import browserify from 'browserify';
import browserSync from 'browser-sync';
import buffer from 'vinyl-buffer';
import c from 'ansi-colors';
import cached from 'gulp-cached';
import del from 'del';
import plumber from 'gulp-plumber';
import pug from 'gulp-pug';
import remember from 'gulp-remember';
import sass from 'gulp-sass';
import source from 'vinyl-source-stream';
import watchify from 'watchify';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'autoprefixer';
import postcss from 'gulp-postcss';
import postcssScss from 'postcss-scss';
import filter from 'gulp-filter';
import data from 'gulp-data';
import path from 'path';
import gulpif from 'gulp-if';
import mergestream from 'merge-stream';
const emitty = require('emitty').setup('src/view', 'pug');

/** CONFIG PATHS */
const paths = {
	sass: {
		src: './src/sass/app.scss',
		watch: './src/sass/**/*.scss',
		dest: './build/asset/style'
	},
	pug: {
		src: './src/view/**/*.pug',
		watch: './src/view/**/*.pug',
		dest: './build/'
	},
	img: {
		src: './src/img/**/*.*',
		dest: './build/asset/img'
	},
	js: {
		src: './src/js/app.js',
		watch: './src/js/**/*.js',
		dest: './build/asset/js'
	},
	icon: {
		dest: './build/img/icon/open-icon/'
	}
};

const env = process.env.NODE_ENV;

const bundle = (bundler) => {
	return bundler
		.transform(babelify, { presets: [ 'env' ] })
		.bundle()
		.on('error', (err) => console.log(c.red.bold(err.message, err.path)))
		.pipe(source('app.js'))
		.pipe(buffer())
		.pipe(sourcemaps.init({ loadMaps: true }))
		.pipe(sourcemaps.write('./maps'))
		.pipe(dest(paths.js.dest))
		.on('end', () => console.log(c.green.bold('JS finished compiling!')));
};

export const buildJs = () => {
	return bundle(browserify(paths.js.src, { debug: true }));
};

export const buildPug = () => {
	let locals = {};
	return src(paths.pug.src, {
		since: lastRun('buildPug')
	})
		.pipe(cached('buildPug'))
		.pipe(plumber())
		.pipe(
			data(function(file) {
				//add relative path on pug file
				locals.relativePath = path.relative(file.base, file.path.replace(/.pug$/, '.html'));
				return locals;
			})
		)
		.pipe(gulpif(global.watch, emitty.stream(global.emittyChangedFile)))
		.pipe(
			pug({
				basedir: './src/view/',
				pretty: true,
				// debug: true,
				data: {
					env: env ? env : 'dev'
				}
			})
		)
		.on('error', (err) => console.log(c.red.bold(err.message, err.path)))
		.pipe(remember('buildPug'))
		.pipe(
			//filter out partials (folders and files starting with "_" )
			filter((file) => {
				return !/\/_/.test(file.path) && !/^_/.test(file.relative);
			})
		)
		.pipe(dest(paths.pug.dest))
		.on('end', () => console.log(c.green.bold('Pug finished compiling!')));
};

export const buildStyle = () =>
	src(paths.sass.src)
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', (err) => console.log(c.red.bold(err.message, err.path))))
		.pipe(postcss([ autoprefixer() ], {syntax: postcssScss}))
		.pipe(sourcemaps.write('./maps'))
		.pipe(dest(paths.sass.dest))
		.on('end', () => console.log(c.green.bold('Sass finished compiling!')));

export const buildImage = () =>
	src(paths.img.src)
		.pipe(dest(paths.img.dest))
		.on('end', () => console.log(c.green.bold('Image finished compiling!')));

export const buildIcons = () => {
	// const openIconic = src(['./node_modules/@icon/open-iconic/open-iconic.*', '!./node_modules/@icon/open-iconic/open-iconic.css'])
	// 	.pipe(dest('./build/asset/img/icon'))
	// 	.on('end', () => console.log(c.green.bold('finished moving open-iconic!')));

	// const openIconicCSS = src('./node_modules/@icon/open-iconic/open-iconic.css')
	// 	.pipe(dest('./build/asset/style'))
	// 	.on('end', () => console.log(c.green.bold('finished moving open-iconic!')));

	// const featherIconJs = src([
	// 	'./node_modules/feather-icons/dist/feather.min.js',
	// 	'./node_modules/feather-icons/dist/feather.min.js.map'
	// ])
	// 	.pipe(dest(paths.js.dest))
	// 	.on('end', () => console.log(c.green.bold('finished moving feather icons!')));

	const featherIcon = src('./node_modules/feather-icons/dist/feather-sprite.svg')
		.pipe(dest(paths.img.dest + '/icon'))
		.on('end', () => console.log(c.green.bold('finished moving feather icons!')));

  // return mergestream(featherIcon);
  return featherIcon;
};

/** Remove build/ folder */
export const clean = () => del([ 'build' ]);

/** DEV SERVER SETUP */
const server = browserSync.create();

export const reload = (done) => {
	server.reload();
	done();
};

export const devServer = (done) => {
	server.init({
		// startPath: 'page/index.html',
		server: {
			baseDir: './build/'
			// directory: true,
		}
	});
	done();
};

/** WATCHER */
export const devWatch = () => {
	// Shows that run "watch" mode
	global.watch = true;

	watch(paths.sass.watch, series(buildStyle, reload));
	watch(paths.img.src, series(buildImage, reload));
	watch(paths.pug.watch, series(buildPug, reload)).on('all', (event, filepath) => {
		global.emittyChangedFile = filepath;
		// console.log(global.emittyChangedFile);
	});

	watchify.args.debug = true;
	let watcher = watchify(browserify(paths.js.src, watchify.args));

	bundle(watcher);

	watcher.on('update', () => {
		bundle(watcher);
	});
};

export const serve = series(
	clean,
	parallel(buildStyle, buildJs, buildImage, buildIcons, buildPug),
	devServer,
	devWatch
);
export const dev = series(clean, parallel(buildStyle, buildJs, buildImage, buildIcons, buildPug));

export default serve;
