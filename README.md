# GULP 4 with PUG + ES6 + SASS(SCSS)
  This is an experiment project with Gulp4

# Start
run `gulp serve` or `npm run start` to start of the dev server. for Production use `npm run build`
# JS
# SASS
  Use BEM for naming convention as well as bootstrap 4 naming (variables, etc)
# PUG
  Extends and Includes pug files doesnt refresh on its own, have to refresh the main file (the file that call those files)
# Gulp
  Using Gulp4 (beta)